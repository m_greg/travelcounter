/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.licznikprzejazdow;

public final class R {
    public static final class array {
        public static final int CityChosserListOptionsArray=0x7f050002;
        public static final int androidcolors=0x7f050000;
        public static final int travelerDialogOptionsArray=0x7f050001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f04000b;
        public static final int blue=0x7f040000;
        public static final int darkblue=0x7f040005;
        public static final int darkgreen=0x7f040007;
        public static final int darkorange=0x7f040008;
        public static final int darkpurple=0x7f040006;
        public static final int darkred=0x7f040009;
        public static final int green=0x7f040002;
        public static final int orange=0x7f040003;
        public static final int purple=0x7f040001;
        public static final int red=0x7f040004;
        public static final int white=0x7f04000a;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int black_gradient=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int actAddEditTravel_buttonAccept=0x7f0a0000;
        public static final int actAddEditTravel_buttonFrom=0x7f0a0006;
        public static final int actAddEditTravel_buttonTo=0x7f0a000a;
        public static final int actAddEditTravel_editTextPrice=0x7f0a000c;
        public static final int actAddEditTravel_textViewDate=0x7f0a000e;
        public static final int actAddEditTravel_textViewFrom=0x7f0a0004;
        public static final int actAddEditTravel_textViewTo=0x7f0a0008;
        public static final int actAddEditTravel_textViewTravelerName=0x7f0a0001;
        public static final int actAddEditTraveler_buttonAccept=0x7f0a000f;
        public static final int actAddEditTraveler_buttonCancel=0x7f0a0010;
        public static final int actAddEditTraveler_editTextBasePayment=0x7f0a0013;
        public static final int actAddEditTraveler_editTextTravelerName=0x7f0a0012;
        public static final int actAddNewCity_buttonAdd=0x7f0a0014;
        public static final int actAddNewCity_editTextCityName=0x7f0a0015;
        public static final int actCityChosser_buttonAdd=0x7f0a0016;
        public static final int actCityChosser_listView=0x7f0a0017;
        public static final int actMain_buttonAdd=0x7f0a0018;
        public static final int actMain_listViewTravelers=0x7f0a0019;
        public static final int actOptionChosserDialog_listView=0x7f0a001a;
        public static final int actTraveler_buttonAdd=0x7f0a001d;
        public static final int actTraveler_listView=0x7f0a001e;
        public static final int actTraveler_textViewSelectedTravels=0x7f0a0020;
        public static final int actTraveler_textViewToPay=0x7f0a0021;
        public static final int action_settings=0x7f0a0025;
        public static final int frameLayout1=0x7f0a001c;
        public static final int layoutListMenuItem_textView=0x7f0a0011;
        public static final int layoutTravelListRow_checkBox=0x7f0a0022;
        public static final int layoutTravelListRow_textViewDate=0x7f0a0003;
        public static final int layoutTravelListRow_textViewFrom=0x7f0a001f;
        public static final int layoutTravelListRow_textViewPrice=0x7f0a0024;
        public static final int layoutTravelListRow_textViewTo=0x7f0a0023;
        public static final int linearLayout1=0x7f0a0002;
        public static final int relativeLayout1=0x7f0a001b;
        public static final int textView1=0x7f0a0009;
        public static final int textView2=0x7f0a0007;
        public static final int textView3=0x7f0a000b;
        public static final int textView4=0x7f0a000d;
        public static final int textView5=0x7f0a0005;
    }
    public static final class layout {
        public static final int activity_add_edit_travel=0x7f030000;
        public static final int activity_add_edit_traveler=0x7f030001;
        public static final int activity_add_new_city=0x7f030002;
        public static final int activity_city_chosser=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int activity_option_chosser_dialog=0x7f030005;
        public static final int activity_traveler=0x7f030006;
        public static final int layout_list_menu_item=0x7f030007;
        public static final int layout_single_column_list_row=0x7f030008;
        public static final int layout_travel_list_row=0x7f030009;
    }
    public static final class menu {
        public static final int add_edit_travel=0x7f090000;
        public static final int add_edit_traveler=0x7f090001;
        public static final int add_new_city=0x7f090002;
        public static final int city_chosser=0x7f090003;
        public static final int main=0x7f090004;
        public static final int option_chosser_dialog=0x7f090005;
        public static final int traveler=0x7f090006;
    }
    public static final class string {
        public static final int Accept=0x7f07000b;
        public static final int Base_payment=0x7f07000a;
        public static final int Cancel=0x7f07000c;
        public static final int Chosse_city=0x7f070014;
        public static final int City_name=0x7f070017;
        public static final int Date=0x7f070011;
        public static final int From=0x7f070012;
        public static final int Price=0x7f070010;
        public static final int To=0x7f070013;
        public static final int Traveler_name=0x7f070009;
        public static final int Wrong_base_payment=0x7f07000e;
        public static final int Wrong_name=0x7f07000d;
        public static final int action_settings=0x7f070002;
        public static final int add=0x7f070004;
        public static final int app_name=0x7f070000;
        public static final int hello_world=0x7f070001;
        public static final int selected=0x7f070006;
        public static final int title_activity_add_edit_travel=0x7f07000f;
        public static final int title_activity_add_edit_traveler=0x7f070008;
        public static final int title_activity_add_new_city=0x7f070016;
        public static final int title_activity_city_chosser=0x7f070015;
        public static final int title_activity_option_chosser_dialog=0x7f070003;
        public static final int title_activity_traveler=0x7f070005;
        public static final int to_pay=0x7f070007;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
    }
}

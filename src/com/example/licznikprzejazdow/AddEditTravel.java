package com.example.licznikprzejazdow;

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
//import android.widget.Toast;

import com.example.licznikprzejazdow.db.TblCity;
import com.example.licznikprzejazdow.db.TblTraveler;

public class AddEditTravel extends Activity implements ActivityTemple {
	// ===========================================================
	// Constants
	// ===========================================================
	public static final String IN_PARM_TRAVELER_ID = "ipti";
	public static final String IN_OUT_PARM_CITY_FROM_ID = "ipcfi";
	public static final String IN_OUT_PARM_CITY_TO_ID = "ipcti";
	public static final String IN_OUT_PARM_PRICE = "ipp";
	public static final String IN_OUT_PARM_DATE = "ipd";

	private static final int REQUEST_CODE_CITY_FROM = 2328;
	private static final int REQUEST_CODE_CITY_TO = 2928;

	private final Date date = new Date();
	// ===========================================================
	// Fields
	// ===========================================================

	private SQLiteDatabase db;

	private TextView travelerNameTextView, fromTextView, toTextView,
			dateTextView;
	private EditText priceEditText;
	private Button fromButton, toButton, acceptButton;

	private long travelerId, cityFromId, cityToId;
	private long dateStamp;
	private float price;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub
		this.db = MainActivity.getInstance().getDb();

		this.travelerId = this.getIntent().getLongExtra(IN_PARM_TRAVELER_ID,
				-1l);
		this.cityFromId = this.getIntent().getLongExtra(
				IN_OUT_PARM_CITY_FROM_ID, -1l);
		this.cityToId = this.getIntent().getLongExtra(IN_OUT_PARM_CITY_TO_ID,
				-1l);
		this.dateStamp = this.getIntent().getLongExtra(IN_OUT_PARM_DATE,
				System.currentTimeMillis());
		this.price = this.getIntent().getFloatExtra(IN_OUT_PARM_PRICE, -1f);
	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stubaved
		this.travelerNameTextView = (TextView) this
				.findViewById(R.id.actAddEditTravel_textViewTravelerName);
		this.fromTextView = (TextView) this
				.findViewById(R.id.actAddEditTravel_textViewFrom);
		this.toTextView = (TextView) this
				.findViewById(R.id.actAddEditTravel_textViewTo);
		this.priceEditText = (EditText) this
				.findViewById(R.id.actAddEditTravel_editTextPrice);
		this.dateTextView = (TextView) this
				.findViewById(R.id.actAddEditTravel_textViewDate);
		this.acceptButton = (Button) this
				.findViewById(R.id.actAddEditTravel_buttonAccept);
		this.fromButton = (Button) this
				.findViewById(R.id.actAddEditTravel_buttonFrom);
		this.toButton = (Button) this
				.findViewById(R.id.actAddEditTravel_buttonTo);

		if (this.travelerId >= 0)
			this.travelerNameTextView.setText(TblTraveler.getTaveler(db,
					this.travelerId).getName());
		if (this.cityFromId >= 0)
			this.fromTextView
					.setText(TblCity.getCity(db, cityFromId).getName());
		if (this.cityToId >= 0)
			this.toTextView.setText(TblCity.getCity(db, cityFromId).getName());
		if (this.price >= 0)
			this.priceEditText.setText(String.valueOf(price));

		Date date = new Date();
		date.setTime(dateStamp);
		DateFormat dateFormat = DateFormat.getDateTimeInstance(
				DateFormat.SHORT, DateFormat.SHORT, MainActivity.LOCALE);
		dateFormat.setTimeZone(MainActivity.TIME_ZONE);
		this.dateTextView.setText(dateFormat.format(date));
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub

		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (arg0 == acceptButton) {

					try {
						price = Float.valueOf(priceEditText.getText()
								.toString());
						Intent i = getIntent();
						i.putExtra(IN_OUT_PARM_CITY_FROM_ID, cityFromId);
						i.putExtra(IN_OUT_PARM_CITY_TO_ID, cityToId);
						i.putExtra(IN_OUT_PARM_PRICE, price);
						i.putExtra(IN_OUT_PARM_DATE, dateStamp);
						setResult(RESULT_OK, i);
						finish();
					} catch (NumberFormatException e) {
						// Toast.makeText(getApplicationContext(),
						// "Wrong price format.", Toast.LENGTH_LONG)
						// .show();
					}

				} else if (arg0 == fromButton) {
					Intent i = new Intent(AddEditTravel.this, CityChosser.class);
					startActivityForResult(i, REQUEST_CODE_CITY_FROM);
				} else if (arg0 == toButton) {
					Intent i = new Intent(AddEditTravel.this, CityChosser.class);
					startActivityForResult(i, REQUEST_CODE_CITY_TO);
				}
			}
		};

		this.acceptButton.setOnClickListener(listener);
		this.fromButton.setOnClickListener(listener);
		this.toButton.setOnClickListener(listener);

	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE_CITY_FROM:
			if (resultCode == RESULT_OK) {
				this.cityFromId = data.getLongExtra(
						CityChosser.OUT_PARM_CITY_ID, -1l);
				TblCity.City c;
				if ((c = TblCity.getCity(db, this.cityFromId)) != null) {
					this.fromTextView.setText(c.getName());
					// Toast.makeText(getApplicationContext(),
					// "ID: " + this.cityFromId, Toast.LENGTH_LONG).show();
				}
			}
			break;
		case REQUEST_CODE_CITY_TO:
			if (resultCode == RESULT_OK) {
				this.cityToId = data.getLongExtra(CityChosser.OUT_PARM_CITY_ID,
						-1l);
				TblCity.City c;
				if ((c = TblCity.getCity(db, this.cityToId)) != null) {
					this.toTextView.setText(c.getName());
					// Toast.makeText(getApplicationContext(), "ID: " +
					// c.getId(),
					// Toast.LENGTH_LONG).show();
				}
			}
			break;
		}
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_edit_travel);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_edit_travel, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}

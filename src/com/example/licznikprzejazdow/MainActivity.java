package com.example.licznikprzejazdow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
//import android.widget.Toast;

import com.example.licznikprzejazdow.db.SqlDbAdapter;
import com.example.licznikprzejazdow.db.TblTraveler;

public class MainActivity extends Activity implements ActivityTemple,
		Handler.Callback {

	// ===========================================================
	// Constants
	// ===========================================================

	// What parameters need to message objects
	private static final int WHAT_LOAD_ALL_TRAVELERS = 10;
	private static final int WHAT_ADD_TRAVELER = 20;
	private static final int WHAT_EDIT_TRAVELER = 30;

	private static MainActivity INSTANCE;
	private static final String LOG_TAG = "From MainActivity";
	private static final String[] DIALOG_OPTIONS = { "Edit", "Remove" };
	private static final int REQUEST_CODE_DIALOG_OPT = 12;

	private static final String PARM_TRAVELER_NAME = "ptn";
	private static final String PARM_TRAVELER_ID = "pti";
	private static final String PARM_TRAVELER_BASE_PAYMENT = "ptbp";

	private static final int REQUEST_CODE_ADD_EDIT_TRAVELER = 231;

	
	//public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("GTM+1");
	public static final Locale LOCALE = Locale.getDefault();
	// ===========================================================
	public static final TimeZone TIME_ZONE = TimeZone.getDefault();
	// Fields
	// ===========================================================

	// Data base adapter - lets to open and close database
	private SqlDbAdapter sqlDbAdapter;

	// SQL database - lets to operate on database
	private SQLiteDatabase db;

	// GUI components
	private Button buttonAdd;
	private ListView listViewTravelers;

	private List<TblTraveler.Traveler> syncTravelersList;
	private ListAdapter listAdapter;

	private Handler handler;

	private Runnable refreshList = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			listAdapter.notifyDataSetChanged();
		}
	};

	// ===========================================================
	// Methods
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.db.close();
		this.handler.getLooper().quit();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE_DIALOG_OPT:
			if (resultCode == RESULT_OK) {
				// Edit
				if (DIALOG_OPTIONS[0]
						.equals(data
								.getStringExtra(OptionChosserDialog.OUT_PARM_CHOSSEN_OPTION))) {
					Intent i = new Intent(this, AddEditTraveler.class);
					i.putExtra(OptionChosserDialog.IN_PARM_OPTIONS_ARRAY,
							DIALOG_OPTIONS);
					i.putExtra(AddEditTraveler.IN_OUT_PARM_MODE,
							AddEditTraveler.MODE_EDIT);
					i.putExtra(AddEditTraveler.IN_OUT_PARM_TRAVELER_ID,
							data.getLongExtra(PARM_TRAVELER_ID, -1l));
					i.putExtra(AddEditTraveler.IN_OUT_PARM_TRAVELER_NAME,
							data.getStringExtra(PARM_TRAVELER_NAME));
					i.putExtra(
							AddEditTraveler.IN_OUT_PARM_TRAVELER_BASE_PAYMENT,
							data.getFloatExtra(PARM_TRAVELER_BASE_PAYMENT, 0f));
					startActivityForResult(i, REQUEST_CODE_ADD_EDIT_TRAVELER);

				} else
				// Remove
				if (DIALOG_OPTIONS[1]
						.equals(data
								.getStringExtra(OptionChosserDialog.OUT_PARM_CHOSSEN_OPTION))) {
					long id = data.getLongExtra(PARM_TRAVELER_ID, -1l);
					if (id >= 0 && TblTraveler.deleteIdx(db, id)) {
						Iterator<TblTraveler.Traveler> i = this.syncTravelersList
								.iterator();
						while (i.hasNext()) {
							if (i.next().getId() == id) {
								i.remove();
								this.listAdapter.notifyDataSetChanged();
								break;
							}
						}
					}
				}
			}
			break;
		case REQUEST_CODE_ADD_EDIT_TRAVELER:

			if (resultCode == RESULT_OK) {

				String name = data
						.getStringExtra(AddEditTraveler.IN_OUT_PARM_TRAVELER_NAME);
				float basePayment = data.getFloatExtra(
						AddEditTraveler.IN_OUT_PARM_TRAVELER_BASE_PAYMENT, 0f);

				// Add option
				if (AddEditTraveler.MODE_ADD.equals(data
						.getStringExtra(AddEditTraveler.IN_OUT_PARM_MODE))) {

					TblTraveler.Traveler traveler = new TblTraveler.Traveler(
							-1, name, basePayment);

					handler.dispatchMessage(Message.obtain(null,
							WHAT_ADD_TRAVELER, traveler));

				} else
				// Edit
				if (AddEditTraveler.MODE_EDIT.equals(data
						.getStringExtra(AddEditTraveler.IN_OUT_PARM_MODE))) {

					long id = data.getLongExtra(
							AddEditTraveler.IN_OUT_PARM_TRAVELER_ID, -1l);
					TblTraveler.Traveler traveler = new TblTraveler.Traveler(
							id, name, basePayment);

					handler.dispatchMessage(Message.obtain(null,
							WHAT_EDIT_TRAVELER, traveler));
				}
			}

			break;
		}
	}

	private void getDataFromDB() {

		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... arg0) {
				// TODO Auto-generated method stub
				syncTravelersList.clear();
				syncTravelersList.addAll(TblTraveler.getAllTravelers(db));
				return null;
			}

			@Override
			protected void onPostExecute(Void o) {
				listAdapter.notifyDataSetChanged();
			}
		}.execute();

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub
		INSTANCE = this;

		HandlerThread ht = new HandlerThread("Background thread.");
		ht.start();
		this.handler = new Handler(ht.getLooper(), this);

		this.sqlDbAdapter = new SqlDbAdapter(this.getApplicationContext());
		this.db = this.sqlDbAdapter.open();
	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.buttonAdd = (Button) this.findViewById(R.id.actMain_buttonAdd);
		this.listViewTravelers = (ListView) this
				.findViewById(R.id.actMain_listViewTravelers);

		this.syncTravelersList = Collections
				.synchronizedList(new ArrayList<TblTraveler.Traveler>());
		// this.getDataFromDB();
		this.listAdapter = new ListAdapter(this,
				R.layout.layout_single_column_list_row, this.syncTravelersList);
		this.listViewTravelers.setAdapter(listAdapter);

		handler.dispatchMessage(Message.obtain(null, WHAT_LOAD_ALL_TRAVELERS));
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub
		OnItemClickListener listListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Object o;
				if ((o = arg0.getItemAtPosition(arg2)) instanceof TblTraveler.Traveler) {
					TblTraveler.Traveler traveler = (TblTraveler.Traveler) o;

					//Toast.makeText(getApplicationContext(), traveler.getName(),
					//		Toast.LENGTH_SHORT).show();

					Intent i = new Intent(getApplicationContext(),
							Traveler.class);
					i.putExtra(Traveler.IN_PARM_TRAVELER_NAME,
							traveler.getName());
					i.putExtra(Traveler.IN_PARM_TRAVELER_ID, traveler.getId());

					MainActivity.this.startActivity(i);
				}
			}
		};

		OnItemLongClickListener longListListener = new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				Object o;
				if ((o = arg0.getItemAtPosition(arg2)) instanceof TblTraveler.Traveler) {
					TblTraveler.Traveler traveler = (TblTraveler.Traveler) o;

					Intent i = new Intent(MainActivity.this,
							OptionChosserDialog.class);
					i.putExtra(OptionChosserDialog.IN_PARM_OPTIONS_ARRAY,
							DIALOG_OPTIONS);
					i.putExtra(PARM_TRAVELER_NAME, traveler.getName());
					i.putExtra(PARM_TRAVELER_ID, traveler.getId());
					i.putExtra(PARM_TRAVELER_BASE_PAYMENT,
							traveler.getBasePayment());
					MainActivity.this.startActivityForResult(i,
							REQUEST_CODE_DIALOG_OPT);

				}
				return true;
			}
		};

		OnClickListener btnListener = new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (arg0 instanceof Button) {
					Intent i = new Intent(MainActivity.this,
							AddEditTraveler.class);
					i.putExtra(AddEditTraveler.IN_OUT_PARM_MODE,
							AddEditTraveler.MODE_ADD);
					startActivityForResult(i, REQUEST_CODE_ADD_EDIT_TRAVELER);
				}
			}
		};

		this.buttonAdd.setOnClickListener(btnListener);

		this.listViewTravelers.setOnItemClickListener(listListener);
		this.listViewTravelers.setLongClickable(true);

		this.listViewTravelers.setOnItemLongClickListener(longListListener);
	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean handleMessage(Message _m) {
		// TODO Auto-generated method stub

		switch (_m.what) {
		case WHAT_LOAD_ALL_TRAVELERS:
			this.syncTravelersList.clear();
			this.syncTravelersList.addAll(TblTraveler.getAllTravelers(db));

			break;
		case WHAT_ADD_TRAVELER:
			TblTraveler.Traveler traveler = (TblTraveler.Traveler) _m.obj;
			long id = TblTraveler.insert(db, traveler.getName(),
					traveler.getBasePayment());
			if (id >= 0) {
				syncTravelersList.add(new TblTraveler.Traveler(id, traveler
						.getName(), traveler.getBasePayment()));
			}
			break;
		case WHAT_EDIT_TRAVELER:
			traveler = (TblTraveler.Traveler) _m.obj;
			if (TblTraveler.update(db, traveler.getId(), traveler.getName(),
					traveler.getBasePayment())) {
				Iterator<TblTraveler.Traveler> itr = syncTravelersList
						.iterator();
				while (itr.hasNext()) {
					if (itr.next().getId() == traveler.getId()) {
						itr.remove();
						break;
					}
				}
				syncTravelersList.add(traveler);
			}

			break;
		}

		runOnUiThread(this.refreshList);
		_m.recycle();
		return true;
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public static MainActivity getInstance() {
		return INSTANCE;
	}

	public SQLiteDatabase getDb() {
		return db;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private class ListAdapter extends ArrayAdapter<TblTraveler.Traveler> {

		private final Activity activity;
		private final int resource;
		private final List<TblTraveler.Traveler> objects;

		/**
		 * 
		 * @param context
		 * @param resource
		 * @param objects
		 * @throws IllegalArgumentException
		 *             - when context parameter is not instance of Activity
		 *             class.
		 */
		public ListAdapter(Context context, int resource,
				List<TblTraveler.Traveler> objects)
				throws IllegalArgumentException {
			super(context, resource, objects);

			if (!(context instanceof Activity))
				throw new IllegalArgumentException(
						"Expected Activity class instence.");

			this.activity = (Activity) context;
			this.resource = resource;
			this.objects = objects;

		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			rowView = convertView;

			if (rowView == null) {
				LayoutInflater layoutInflater = activity.getLayoutInflater();
				rowView = layoutInflater.inflate(resource, null, true);
			}

			if (rowView instanceof TextView) {
				TextView tw = (TextView) rowView;
				tw.setText(this.objects.get(position).getName());
			} else
				System.out.println(rowView.getClass().getName());

			return rowView;
		}
	}

}

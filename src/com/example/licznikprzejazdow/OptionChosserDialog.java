package com.example.licznikprzejazdow;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class OptionChosserDialog extends Activity implements ActivityTemple {

	public static final String IN_PARM_OPTIONS_ARRAY = "poa";
	public static final String OUT_PARM_CHOSSEN_OPTION = "co";
	public static final String OUT_PARM_CHOSSEN_OPTION_INDEX = "coi";

	private ListView listview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_option_chosser_dialog);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.option_chosser_dialog, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.listview = (ListView) this
				.findViewById(R.id.actOptionChosserDialog_listView);

		String[] options = this.getIntent().getStringArrayExtra(
				IN_PARM_OPTIONS_ARRAY);

		if (options != null) {
			this.listview.setAdapter(new ListAdapter(this,
					R.layout.layout_list_menu_item, options));
		}
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub
		OnItemClickListener onItemListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Object o;
				if ((o = arg0.getAdapter().getItem(arg2)) instanceof String) {
					Intent i = getIntent();
					i.putExtra(OUT_PARM_CHOSSEN_OPTION, (String) o);
					i.putExtra(OUT_PARM_CHOSSEN_OPTION_INDEX, arg2);
					setResult(RESULT_OK, i);
					finish();
				}

			}
		};
		this.listview.setOnItemClickListener(onItemListener);
	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	private class ListAdapter extends ArrayAdapter<String> {

		private final Activity activity;
		private final int resource;
		private final String[] objects;

		public ListAdapter(Context context, int resource, String[] objects)
				throws IllegalArgumentException {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
			if (!(context instanceof Activity))
				throw new IllegalArgumentException(
						"Expected Activity instance.");

			this.activity = (Activity) context;
			this.resource = resource;
			this.objects = objects;
		}

		public View getView(int position, View contentView, ViewGroup parent) {
			View rowView = contentView;

			if (rowView == null) {
				LayoutInflater layoutInflater = this.activity
						.getLayoutInflater();
				rowView = layoutInflater.inflate(resource, null, true);
				rowView.setTag(rowView
						.findViewById(R.id.layoutListMenuItem_textView));
			}

			((TextView) rowView.getTag()).setText(objects[position]);

			return rowView;
		}

	}
}

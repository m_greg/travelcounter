package com.example.licznikprzejazdow;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
//import android.widget.Toast;

import com.example.licznikprzejazdow.db.TblCity;

public class CityChosser extends Activity implements ActivityTemple {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int REQUEST_NEW_CITY = 102;
	private static final int REQUEST_LIST_OPTIONS = 121;

	public static final String OUT_PARM_CITY_ID = "upci";
	private static final String PARM_CITY_NAME = "pcm";

	// ===========================================================
	// Fields

	// ===========================================================
	private Button buttonAdd;
	private ListView list;

	private SQLiteDatabase db;
	private ListAdapter listAdapter;
	private List<TblCity.City> cityList;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub
		this.db = MainActivity.getInstance().getDb();
		this.cityList = new ArrayList<TblCity.City>(TblCity.getAllCity(db));
		this.listAdapter = new ListAdapter(this,
				R.layout.layout_single_column_list_row, this.cityList);

	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.buttonAdd = (Button) this
				.findViewById(R.id.actCityChosser_buttonAdd);
		this.list = (ListView) this.findViewById(R.id.actCityChosser_listView);
		this.list.setAdapter(listAdapter);
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (arg0 == buttonAdd) {
					Intent i = new Intent(CityChosser.this, AddNewCity.class);
					startActivityForResult(i, REQUEST_NEW_CITY);
				}
			}

		};

		OnItemClickListener itemClick = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(),
					//	arg0.getAdapter().getItem(arg2).getClass().toString(),
					//	Toast.LENGTH_SHORT).show();

				Object o;
				if ((o = arg0.getAdapter().getItem(arg2)) instanceof TblCity.City) {
					Intent i = getIntent();
					TblCity.City c = (TblCity.City) o;
					i.putExtra(OUT_PARM_CITY_ID, c.getId());
					setResult(RESULT_OK, i);
					finish();
				}

			}
		};

		OnItemLongClickListener longItemClick = new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				Object o;
				if ((o = arg0.getAdapter().getItem(arg2)) instanceof TblCity.City) {
					TblCity.City c = (TblCity.City) o;
					Intent i = new Intent(getApplicationContext(),
							OptionChosserDialog.class);
					i.putExtra(
							OptionChosserDialog.IN_PARM_OPTIONS_ARRAY,
							CityChosser.this.getResources().getStringArray(
									R.array.CityChosserListOptionsArray));
					i.putExtra(OUT_PARM_CITY_ID, c.getId());
					startActivityForResult(i, REQUEST_LIST_OPTIONS);
				}

				return true;
			}
		};

		this.buttonAdd.setOnClickListener(listener);
		this.list.setOnItemClickListener(itemClick);
		this.list.setOnItemLongClickListener(longItemClick);
	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city_chosser);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.city_chosser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		super.onResume();
		this.updateListViewContent();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_NEW_CITY:
			if (resultCode == RESULT_OK) {

				// Testing city name
				String cityName = data
						.getStringExtra(AddNewCity.OUT_PARM_CITY_NAME);
				boolean cityNameCorrect = true;

				if (cityNameCorrect) {
					if (TblCity.insert(db, cityName) >= 0) {
						this.updateListViewContent();
					}
				}
			}
			break;
		case REQUEST_LIST_OPTIONS:
			if (resultCode == RESULT_OK) {
				String[] array = CityChosser.this.getResources()
						.getStringArray(R.array.CityChosserListOptionsArray);
				String s = data
						.getStringExtra(OptionChosserDialog.OUT_PARM_CHOSSEN_OPTION);
				// Remove option was chossed.
				if (array[0].equals(s)) {
					if (TblCity.deleteIdx(db,
							data.getLongExtra(OUT_PARM_CITY_ID, -1l))) {
						updateListViewContent();
					}
				}
			}
			break;
		}

	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void updateListViewContent() {
		this.cityList.clear();
		this.cityList.addAll(TblCity.getAllCity(db));
		this.listAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private class ListAdapter extends ArrayAdapter<TblCity.City> {

		private final Activity activity;
		private final int resource = R.layout.layout_single_column_list_row;
		private final List<TblCity.City> objects;

		public ListAdapter(Context context, int resource,
				List<TblCity.City> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub

			if (!(context instanceof Activity))
				throw new IllegalArgumentException(
						"Expected Activity instance.");
			this.activity = (Activity) context;
			this.objects = objects;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater layoutInflater = this.activity
						.getLayoutInflater();
				rowView = layoutInflater.inflate(resource, null, true);
			}

			TextView tv = (TextView) rowView;
			tv.setText(this.objects.get(position).getName());

			return rowView;
		}

	}

}

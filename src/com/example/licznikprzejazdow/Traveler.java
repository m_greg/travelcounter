package com.example.licznikprzejazdow;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
//import android.widget.Toast;

import com.example.licznikprzejazdow.db.TblCity;
import com.example.licznikprzejazdow.db.TblTravel;
import com.example.licznikprzejazdow.db.TblTraveler;

import java.text.DateFormat;

public class Traveler extends Activity implements ActivityTemple {

	// ===========================================================
	// Constants
	// ===========================================================
	private final String LOG_TAG = "TravelView";

	public static final String IN_PARM_TRAVELER_NAME = "ptn";
	public static final String IN_PARM_TRAVELER_ID = "pti";
	private final String PARM_TRAVEL_INDEX = "pti";

	private final String fromAls = "col_from";
	private final String toAls = "col_to";
	private final String fromTbl = "from_tbl";
	private final String toTbl = "to_tbl";

	private final int REQUEST_ADD_NEW_TRAVEL = 101;
	private final int REQUEST_DIALOG_OPIONS_TRAVEL = 103;

	// Date, time, zone, and local attributes need for presenting on list:
	private final int dateStyle = DateFormat.SHORT;
	private final int timeStyle = DateFormat.SHORT;
	// ===========================================================
	// Fields
	// ===========================================================

	private Button buttonAdd;
	private ListView listView;
	private TextView textViewSelected, textViewToPay;
	private String travelerName;
	String select = this.selectTravels();

	private long travelerId;

	private SQLiteDatabase db;
	private LinkedList<TravelWrap> travelList;
	private ListAdapter listAdapter;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub
		this.db = MainActivity.getInstance().getDb();
		this.travelList = new LinkedList<TravelWrap>();
		this.travelerId = this.getIntent().getLongExtra(IN_PARM_TRAVELER_ID,
				-1l);
		this.listAdapter = new ListAdapter(this,
				R.layout.layout_travel_list_row, this.travelList);

		String select = this.selectTravels();
		Cursor c = this.db.rawQuery(select, null);
		if (c != null && c.moveToFirst())
			do {
				TravelWrap wt = new TravelWrap();
				wt.id = c.getLong(c.getColumnIndex(TblTravel.COL_KEY_ID));
				wt.from = c.getString(c.getColumnIndex(fromAls));
				wt.to = c.getString(c.getColumnIndex(toAls));
				wt.paid = c.getInt(c
						.getColumnIndex(TblTravel.COL_PAID_STATE_NAME)) == 1 ? true
						: false;
				wt.payment = c.getFloat(c
						.getColumnIndex(TblTravel.COL_PAYMENT_NAME));
				wt.date = c.getLong(c.getColumnIndex(TblTravel.COL_DATE_NAME));
				this.travelList.add(wt);
			} while (c.moveToNext());

	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.buttonAdd = (Button) this.findViewById(R.id.actTraveler_buttonAdd);
		this.listView = (ListView) this.findViewById(R.id.actTraveler_listView);
		this.textViewSelected = (TextView) this
				.findViewById(R.id.actTraveler_textViewSelectedTravels);
		this.textViewToPay = (TextView) this
				.findViewById(R.id.actTraveler_textViewToPay);

		this.listView.setAdapter(listAdapter);
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub

		OnClickListener clickListener = new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (arg0 == buttonAdd) {
					float price = 0f;
					TblTraveler.Traveler t = TblTraveler.getTaveler(db,
							travelerId);
					if (t != null)
						price = t.getBasePayment();
					Intent i = new Intent(Traveler.this, AddEditTravel.class);
					i.putExtra(AddEditTravel.IN_PARM_TRAVELER_ID, travelerId);
					i.putExtra(AddEditTravel.IN_OUT_PARM_CITY_FROM_ID, -1l);
					i.putExtra(AddEditTravel.IN_OUT_PARM_CITY_TO_ID, -1l);
					i.putExtra(AddEditTravel.IN_OUT_PARM_DATE,
							System.currentTimeMillis());
					i.putExtra(AddEditTravel.IN_OUT_PARM_PRICE, price);
					startActivityForResult(i, REQUEST_ADD_NEW_TRAVEL);
				}
			}
		};

		OnItemClickListener onItemClick = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				TravelWrap tw = listAdapter.getItem(arg2);
				tw.selected = !tw.selected;
				listAdapter.notifyDataSetChanged();
				updateInfoBar();
				// Toast.makeText(getApplicationContext(), "Click",
				// Toast.LENGTH_SHORT).show();
			}
		};

		OnItemLongClickListener itemLongClick = new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				// Toast.makeText(getApplicationContext(), "Long click",
				// Toast.LENGTH_SHORT).show();
				Intent i = new Intent(Traveler.this, OptionChosserDialog.class);
				i.putExtra(
						OptionChosserDialog.IN_PARM_OPTIONS_ARRAY,
						Traveler.this.getResources().getStringArray(
								R.array.travelerDialogOptionsArray));
				i.putExtra(PARM_TRAVEL_INDEX, arg2);
				startActivityForResult(i, REQUEST_DIALOG_OPIONS_TRAVEL);
				return true;
			}
		};

		this.buttonAdd.setOnClickListener(clickListener);
		this.listView.setOnItemClickListener(onItemClick);
		this.listView.setOnItemLongClickListener(itemLongClick);

	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto generated method stub
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_ADD_NEW_TRAVEL:
			if (resultCode == RESULT_OK) {
				long travelerId = data.getLongExtra(
						AddEditTravel.IN_PARM_TRAVELER_ID, -1l);
				long cityFromId = data.getLongExtra(
						AddEditTravel.IN_OUT_PARM_CITY_FROM_ID, -1l);
				long cityToId = data.getLongExtra(
						AddEditTravel.IN_OUT_PARM_CITY_TO_ID, -1l);
				long date = data.getLongExtra(AddEditTravel.IN_OUT_PARM_DATE,
						0l);
				float price = data.getFloatExtra(
						AddEditTravel.IN_OUT_PARM_PRICE, 0f);

				//
				TblTravel.Travel traveler = new TblTravel.Travel(cityFromId,
						cityToId, travelerId, price, date, false);

				long travelId;
				if ((travelId = TblTravel.insertTravel(db, traveler)) >= 0) {

					// Getting city names:
					String cityFrom = TblCity.getCity(db, cityFromId).getName();
					String cityTo = TblCity.getCity(db, cityToId).getName();

					// Create new travel wrap:
					TravelWrap tw = new TravelWrap(travelId, cityFrom, cityTo,
							traveler.isPayd(), false, price, date);

					// Add travel wrap to travel list:
					this.travelList.addFirst(tw);

					// Refresh travels list:
					this.listAdapter.notifyDataSetChanged();
				}

			}
			break;
		case REQUEST_DIALOG_OPIONS_TRAVEL:
			if (resultCode == RESULT_OK) {
				int optionIndex = data.getIntExtra(
						OptionChosserDialog.OUT_PARM_CHOSSEN_OPTION_INDEX, -1);
				switch (optionIndex) {
				case 0:// Select all
					for (TravelWrap tw : this.travelList)
						tw.selected = true;
					this.listAdapter.notifyDataSetChanged();
					this.updateInfoBar();
					break;
				case 1:// Unselect all
					for (TravelWrap tw : this.travelList)
						tw.selected = false;
					this.listAdapter.notifyDataSetChanged();
					this.updateInfoBar();
					break;
				case 2:// Remove selected
					Iterator<TravelWrap> twi = this.travelList.iterator();
					while (twi.hasNext()) {
						TravelWrap tw = twi.next();
						if (tw.selected)
							if (TblTravel.deleteIdx(db, tw.id))
								twi.remove();
					}
					this.listAdapter.notifyDataSetChanged();
					this.updateInfoBar();
					break;
				case 3:// Pay selected
					twi = this.travelList.iterator();
					while (twi.hasNext()) {
						TravelWrap tw = twi.next();
						if (tw.selected) {
							Log.i(LOG_TAG, "Id to pay: " + tw.id);

							TblTravel.Travel t = TblTravel.getTravel(db, tw.id);

							boolean travelUpdate = TblTravel.update(db,
									t.getId(), t.getFromId(), t.getToId(),
									t.getTravelerId(), t.getPayment(),
									t.getDate(), tw.selected);
							if (travelUpdate)
								twi.remove();
						}
					}
					this.listAdapter.notifyDataSetChanged();
					this.updateInfoBar();
					break;
				}
			}
			break;
		}
	}

	protected void onResume() {
		super.onResume();
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_traveler);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.traveler, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// Select travels string.
	private String selectTravels() {
		final StringBuilder s = new StringBuilder();

		s.append("SELECT  ").append(fromTbl).append('.')
				.append(TblCity.COL_CITY_NAME).append(" AS ").append(fromAls)
				.append(", ");
		s.append(toTbl).append('.').append(TblCity.COL_CITY_NAME)
				.append(" AS ").append(toAls);
		s.append(", ").append(TblTravel.COL_KEY_ID);
		s.append(", ").append(TblTravel.COL_PAID_STATE_NAME);
		s.append(", ").append(TblTravel.COL_PAYMENT_NAME);
		s.append(", ").append(TblTravel.COL_DATE_NAME);
		s.append(", ").append(TblTravel.COL_TRAVELER_KEY_NAME);
		s.append(" FROM ").append(TblTravel.TAB_NAME);

		s.append(" LEFT JOIN ").append(TblCity.TAB_NAME).append(' ')
				.append(fromTbl).append(" ON ").append(TblTravel.TAB_NAME)
				.append('.').append(TblTravel.COL_FROM_KEY_NAME).append(" = ")
				.append(fromTbl).append('.').append(TblCity.COL_KEY_ID);

		s.append(" LEFT JOIN ").append(TblCity.TAB_NAME).append(' ')
				.append(toTbl).append(" ON ").append(TblTravel.TAB_NAME)
				.append('.').append(TblTravel.COL_TO_KEY_NAME).append(" = ")
				.append(toTbl).append('.').append(TblCity.COL_KEY_ID);

		s.append(" WHERE ").append(TblTravel.TAB_NAME).append('.')
				.append(TblTravel.COL_TRAVELER_KEY_NAME).append(" = ")
				.append(String.valueOf(this.travelerId));
		s.append(" AND ");
		s.append(TblTravel.COL_PAID_STATE_NAME).append(" = 0");
		s.append(" ORDER BY ").append(TblTravel.COL_DATE_NAME).append(" DESC");
		s.append(';');
		return s.toString();
	}

	private void updateInfoBar() {
		int selected = 0;
		float toPay = 0f;
		for (TravelWrap tw : this.travelList) {
			if (tw.selected) {
				selected++;
				toPay += tw.payment;
			}
		}

		this.textViewSelected.setText(String.valueOf(selected));
		this.textViewToPay.setText(toPay + " zł");
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// =========================================================

	private class ListAdapter extends ArrayAdapter<TravelWrap> {

		private Activity activity;
		private int resource;
		private List<TravelWrap> objects;
		private final Date date = new Date();
		private DateFormat dateFormat;

		public ListAdapter(Context context, int resource,
				List<TravelWrap> objects) {
			super(context, resource, objects);

			if (!(context instanceof Activity))
				throw new IllegalArgumentException(
						"Expected Activity instance, found: "
								+ context.getClass().getName());

			this.activity = (Activity) context;
			this.resource = resource;
			this.objects = objects;

			this.dateFormat = DateFormat.getDateTimeInstance(dateStyle,
					timeStyle, MainActivity.LOCALE);
			this.dateFormat.setTimeZone(MainActivity.TIME_ZONE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			Holder holder = null;

			if (rowView == null) {
				LayoutInflater layoutInflater = activity.getLayoutInflater();
				rowView = layoutInflater.inflate(resource, null, true);

				holder = new Holder();
				holder.checkBox = (CheckBox) rowView
						.findViewById(R.id.layoutTravelListRow_checkBox);
				holder.from = (TextView) rowView
						.findViewById(R.id.layoutTravelListRow_textViewFrom);
				holder.to = (TextView) rowView
						.findViewById(R.id.layoutTravelListRow_textViewTo);
				holder.date = (TextView) rowView
						.findViewById(R.id.layoutTravelListRow_textViewDate);
				holder.price = (TextView) rowView
						.findViewById(R.id.layoutTravelListRow_textViewPrice);
				rowView.setTag(holder);
			} else {
				holder = (Holder) rowView.getTag();
			}

			TravelWrap tw = this.objects.get(position);

			holder.checkBox.setChecked(tw.selected);
			holder.from.setText(tw.from);
			holder.to.setText(tw.to);
			date.setTime(tw.date);
			holder.date.setText(this.dateFormat.format(date));
			holder.price.setText(String.valueOf(tw.payment) + " zł");

			return rowView;
		}

		private class Holder {
			public CheckBox checkBox;
			public TextView from, to, date, price;
		}

	}

	private class TravelWrap {
		public long id;
		public String from, to;
		public boolean paid;
		public boolean selected;
		public float payment;
		public long date;

		public TravelWrap() {

		};

		public TravelWrap(long id, String from, String to, boolean paid,
				boolean selected, float payment, long _date) {
			super();
			this.id = id;
			this.from = from;
			this.to = to;
			this.paid = paid;
			this.selected = selected;
			this.payment = payment;
			this.date = _date;
		}

	}
}

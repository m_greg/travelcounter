package com.example.licznikprzejazdow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
//import android.widget.Toast;

public class AddEditTraveler extends Activity implements ActivityTemple {

	// ===========================================================
	// Constants
	// ===========================================================

	public static final String IN_OUT_PARM_MODE = "pm";
	public static final String MODE_ADD = "ma";
	public static final String MODE_EDIT = "me";

	public static final String IN_OUT_PARM_TRAVELER_ID = "pti";
	public static final String IN_OUT_PARM_TRAVELER_NAME = "ptn";
	public static final String IN_OUT_PARM_TRAVELER_BASE_PAYMENT = "ptbp";

	// ===========================================================
	// Fields
	// ===========================================================

	private Button buttonAccept, buttonCancel;
	private EditText editTextTravelerName, editTextTravelerBasePayment;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.buttonAccept = (Button) this
				.findViewById(R.id.actAddEditTraveler_buttonAccept);
		this.buttonCancel = (Button) this
				.findViewById(R.id.actAddEditTraveler_buttonCancel);
		this.editTextTravelerName = (EditText) this
				.findViewById(R.id.actAddEditTraveler_editTextTravelerName);
		this.editTextTravelerBasePayment = (EditText) this
				.findViewById(R.id.actAddEditTraveler_editTextBasePayment);

		this.editTextTravelerBasePayment.setText(String.valueOf(getIntent()
				.getFloatExtra(IN_OUT_PARM_TRAVELER_BASE_PAYMENT, 0f)));
		this.editTextTravelerName.setText(getIntent().getStringExtra(
				IN_OUT_PARM_TRAVELER_NAME));
	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub
		OnClickListener l = new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (buttonAccept == arg0) {
					if ("".equals(editTextTravelerName.getText().toString())) {
						//Toast.makeText(getApplicationContext(),
						//		getText(R.string.Wrong_name),
						//		Toast.LENGTH_SHORT).show();
						return;
					}

					float basePayment;
					try {
						basePayment = Float.valueOf(editTextTravelerBasePayment
								.getText().toString());
					} catch (NumberFormatException e) {
						//Toast.makeText(getApplicationContext(),
						//		getText(R.string.Wrong_base_payment),
						//		Toast.LENGTH_SHORT).show();
						return;
					}

					Intent i = getIntent();
					i.putExtra(IN_OUT_PARM_TRAVELER_NAME,
							editTextTravelerName.getText().toString());
					i.putExtra(IN_OUT_PARM_TRAVELER_BASE_PAYMENT, basePayment);

					setResult(RESULT_OK, i);
					finish();

				} else if (buttonCancel == arg0) {
					setResult(RESULT_CANCELED, getIntent());
					finish();
				}
			}
		};

		this.buttonAccept.setOnClickListener(l);
		this.buttonCancel.setOnClickListener(l);
	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_edit_traveler);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_edit_traveler, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}

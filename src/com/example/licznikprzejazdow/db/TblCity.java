package com.example.licznikprzejazdow.db;

import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblCity {

	public static final String TAB_NAME = "city";

	// Column id - it is primary key
	public static final String COL_KEY_ID = "id_city";
	public static final String COL_KEY_TYPE = "INTEGER";
	public static final String COL_KEY_TYPE_OPT = "PRIMARY KEY AUTOINCREMENT";
	public static final int COL_KEY_COLUMN_IDX = 0;

	// Column city name.
	public static final String COL_CITY_NAME = "city_name";
	public static final String COL_CITY_DESC = "City name.";
	public static final String COL_CITY_TYPE = "VARCHAR(20)";
	public static final String COL_CITY_TYPE_OPT = "NOT NULL";
	public static final int COL_CITY_COLUMN_IDX = 1;

	public static String createTableString() {
		String[][] allColParm = {
				{ COL_KEY_ID, COL_KEY_TYPE, COL_KEY_TYPE_OPT },
				{ COL_CITY_NAME, COL_CITY_TYPE, COL_CITY_TYPE_OPT } };

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ");
		sb.append(TAB_NAME);
		sb.append(" ( ");
		for (int colIdx = 0; colIdx < allColParm.length; colIdx++) {
			for (int parmIdx = 0; parmIdx < allColParm[0].length; parmIdx++) {
				sb.append(allColParm[colIdx][parmIdx]);
				if (parmIdx < allColParm[0].length - 1)
					sb.append(' ');
			}
			if (colIdx < allColParm.length - 1)
				sb.append(", ");
		}
		sb.append(");");

		return sb.toString();
	}

	public static String dropTableString() {
		return "DROP TABLE IF EXISTS " + TAB_NAME;
	}

	public static String[] createRelationsString() {
		return new String[0];
	}

	public static long insert(SQLiteDatabase _db, String _city_name) {
		ContentValues newRow = new ContentValues();
		newRow.put(COL_CITY_NAME, _city_name);

		return _db.insert(TAB_NAME, null, newRow);
	}

	public static long insertCity(SQLiteDatabase _db, TblCity.City _city) {
		return insert(_db, _city.getName());
	}

	public static boolean update(SQLiteDatabase _db, long _id_city,
			String _city_name) {
		String where = COL_KEY_ID + "=" + _id_city;
		ContentValues newRow = new ContentValues();
		newRow.put(COL_CITY_NAME, _city_name);

		return _db.update(TAB_NAME, newRow, where, null) > 0;
	}

	public static boolean deleteIdx(SQLiteDatabase _db, long id_city) {
		boolean value = false;
		String removeStr = TblTravel.COL_FROM_KEY_NAME + "=" + id_city + " OR "
				+ TblTravel.COL_TO_KEY_NAME + "=" + id_city;
		String where = COL_KEY_ID + "=" + id_city;


		_db.delete(TblTravel.TAB_NAME, removeStr, null);
		value = _db.delete(TAB_NAME, where, null) > 0;


		return value;
	}

	public static void deleteAll(SQLiteDatabase _db) {
		throw new UnsupportedOperationException();
	}

	public static Cursor getAll(SQLiteDatabase _db) {
		String[] columns = { COL_KEY_ID, COL_CITY_NAME };
		return _db.query(TAB_NAME, columns, null, null, null, null, null);
	}

	public static List<TblCity.City> getAllCity(SQLiteDatabase _db) {
		Cursor c = getAll(_db);
		List<TblCity.City> list = new LinkedList<TblCity.City>();

		if (c != null && c.moveToFirst())
			do {
				list.add(new City(c.getLong(COL_KEY_COLUMN_IDX), c
						.getString(COL_CITY_COLUMN_IDX)));
			} while (c.moveToNext());
		return list;
	}

	public static Cursor get(SQLiteDatabase _db, long _idx) {
		String[] columns = { COL_KEY_ID, COL_CITY_NAME };
		String where = COL_KEY_ID + "=" + _idx;
		return _db.query(TAB_NAME, columns, where, null, null, null, null);
	}

	public List<TblCity.City> getAllCities(SQLiteDatabase _db) {
		Cursor c = getAll(_db);
		List<TblCity.City> cities = new LinkedList<TblCity.City>();
		if (c != null && c.moveToFirst())
			do {
				cities.add(new City(c.getLong(COL_KEY_COLUMN_IDX), c
						.getString(COL_CITY_COLUMN_IDX)));
			} while (c.moveToNext());

		return cities;
	}

	public static TblCity.City getCity(SQLiteDatabase _db, long _idx) {
		Cursor c = get(_db, _idx);
		if (c != null && c.moveToFirst())
			return new City(c.getLong(COL_KEY_COLUMN_IDX),
					c.getString(COL_CITY_COLUMN_IDX));
		return null;
	}

	public static final class City {

		private final long id;
		private final String name;

		public City(long _id, String _name) {
			this.id = _id;
			this.name = _name;
		}

		public long getId() {
			return id;
		}

		public String getName() {
			return name;
		}
	}

}
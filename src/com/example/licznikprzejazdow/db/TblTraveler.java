package com.example.licznikprzejazdow.db;

import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblTraveler {

	public static final String TAB_NAME = "traveler";

	// Column id - it is a primary key
	public static final String COL_KEY_ID = "id_traveler";
	public static final String COL_KEY_TYPE = "INTEGER";
	public static final String COL_KEY_TYPE_OPT = "PRIMARY KEY AUTOINCREMENT";
	public static final int COL_KEY_COLUMN_IDX = 0;

	// Column name -
	public static final String COL_NAME = "name";
	public static final String COL_NAME_DESC = "Traveler name.";
	public static final String COL_NAME_TYPE = "VARCHAR(15)";
	public static final String COL_NAME_TYPE_OPT = "NOT NULL";
	public static final int COL_NAME_COLUMN_IDX = 1;

	// Column payment -
	public static final String COL_BASE_PAYMENT_NAME = "payment";
	public static final String COL_BASE_PAYMENT_DESC = "Default traveler's payment.";
	public static final String COL_BASE_PAYMENT_TYPE = "DECIMAL(2,2)";
	public static final String COL_BASE_PAYMENT_TYPE_OPT = "NOT NULL";
	public static final int COL_BASE_PAYMENT_COLUMN_IDX = 2;

	public static String createTableString() {
		String[][] allColParm = {
				{ COL_KEY_ID, COL_KEY_TYPE, COL_KEY_TYPE_OPT },
				{ COL_NAME, COL_NAME_TYPE, COL_NAME_TYPE_OPT },
				{ COL_BASE_PAYMENT_NAME, COL_BASE_PAYMENT_TYPE,
						COL_BASE_PAYMENT_TYPE_OPT } };

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ");
		sb.append(TAB_NAME);
		sb.append(" ( ");
		for (int colIdx = 0; colIdx < allColParm.length; colIdx++) {
			for (int parmIdx = 0; parmIdx < allColParm[colIdx].length; parmIdx++) {
				sb.append(allColParm[colIdx][parmIdx]);
				if (parmIdx < allColParm[colIdx].length - 1)
					sb.append(' ');
			}
			if (colIdx < allColParm.length - 1)
				sb.append(", ");
		}
		sb.append(");");

		return sb.toString();
	}

	public static String dropTableString() {
		return "DROP TABLE IF EXISTS " + TAB_NAME;
	}

	public static String[] createRelationsString() {
		return new String[0];
	}

	public static long insert(SQLiteDatabase _db, String _name,
			float _basePayment) {
		ContentValues newRow = new ContentValues();
		newRow.put(COL_NAME, _name);
		newRow.put(COL_BASE_PAYMENT_NAME, _basePayment);

		return _db.insert(TAB_NAME, null, newRow);
	}

	public static boolean update(SQLiteDatabase _db, long _id_traveler,
			String _name, float _basePayment) {
		String where = COL_KEY_ID + "=" + _id_traveler;
		ContentValues newRow = new ContentValues();
		newRow.put(COL_NAME, _name);
		newRow.put(COL_BASE_PAYMENT_NAME, _basePayment);

		return _db.update(TAB_NAME, newRow, where, null) >= 0;
	}

	public static boolean deleteIdx(SQLiteDatabase _db, long id_traveler) {
		boolean value = false;

		// First must be removed all travels from table "TblTravel" which column
		// "COL_TRAVELER_KEY_NAME" value equals "id_traveler".
		String removeStr = TblTravel.COL_TRAVELER_KEY_NAME + " = " + id_traveler;
		String where = COL_KEY_ID + " = " + id_traveler;
		_db.delete(TblTravel.TAB_NAME, removeStr, null);
		value = _db.delete(TAB_NAME, where, null) >= 0;

		return value;
	}

	public static void deleteAll(SQLiteDatabase _db) {
		throw new UnsupportedOperationException();
	}

	public static Cursor getAll(SQLiteDatabase _db) {
		String[] columns = { COL_KEY_ID, COL_NAME, COL_BASE_PAYMENT_NAME };
		return _db.query(TAB_NAME, columns, null, null, null, null, null);
	}

	public static Cursor get(SQLiteDatabase _db, long _idx) {
		String[] columns = { COL_KEY_ID, COL_NAME, COL_BASE_PAYMENT_NAME };
		String where = COL_KEY_ID + "=" + _idx;
		return _db.query(TAB_NAME, columns, where, null, null, null, null);
	}

	public static List<TblTraveler.Traveler> getAllTravelers(SQLiteDatabase _db) {
		Cursor c = getAll(_db);
		List<TblTraveler.Traveler> travelersList = new LinkedList<TblTraveler.Traveler>();
		if (c != null && c.moveToFirst()) {
			do {
				travelersList.add(new Traveler(c.getLong(COL_KEY_COLUMN_IDX), c
						.getString(COL_NAME_COLUMN_IDX), c
						.getFloat(COL_BASE_PAYMENT_COLUMN_IDX)));
			} while (c.moveToNext());
		}

		return travelersList;
	}

	public static TblTraveler.Traveler getTaveler(SQLiteDatabase _db, long _idx) {
		Cursor c = get(_db, _idx);
		if (c != null && c.moveToFirst())
			return new Traveler(c.getLong(COL_KEY_COLUMN_IDX),
					c.getString(COL_NAME_COLUMN_IDX),
					c.getFloat(COL_BASE_PAYMENT_COLUMN_IDX));
		return null;
	}

	public static final class Traveler {
		private final long id;
		private final String name;
		private final float basePayment;

		public Traveler(long _id, String _name, float _basePaymenet) {
			this.id = _id;
			this.name = _name;
			this.basePayment = _basePaymenet;
		}

		public long getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public float getBasePayment() {
			return basePayment;
		}

	}
}
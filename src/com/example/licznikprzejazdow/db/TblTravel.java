package com.example.licznikprzejazdow.db;

import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblTravel {

	public static final String TAB_NAME = "travel";

	// Column id - it is a primary key
	public static final String COL_KEY_ID = "id_travel";
	public static final String COL_KEY_TYPE = "INTEGER";
	public static final String COL_KEY_TYPE_OPT = "PRIMARY KEY AUTOINCREMENT";
	public static final int COL_KEY_COLUMN_IDX = 0;

	// Column from - its value is a primary key from table City.
	public static final String COL_FROM_KEY_NAME = "id_from";
	public static final String COL_FROM_KEY_TYPE = TblCity.COL_KEY_TYPE;
	public static final String COL_FROM_KEY_TYPE_OPT = "";
	// public static final String COL_FROM_FOREIGN_KEY_NAME = "from_fk";
	public static final int COL_FROM_COLUMN_IDX = 1;

	// Column to - its value is a primary key from table City.
	public static final String COL_TO_KEY_NAME = "id_to";
	public static final String COL_TO_KEY_TYPE = TblCity.COL_KEY_TYPE;
	public static final String COL_TO_KEY_TYPE_OPT = "";
	public static final int COL_TO_COLUMN_IDX = 2;

	// Column traveler - its value is a primary key from table Traveler;
	public static final String COL_TRAVELER_KEY_NAME = TblTraveler.COL_KEY_ID;
	public static final String COL_TRAVELER_KEY_TYPE = TblTraveler.COL_KEY_TYPE;
	public static final String COL_TRAVELER_KEY_TYPE_OPT = "NOT NULL";
	public static final int COL_TRAVELER_KEY_COLUMN_IDX = 3;

	// Column payment -
	public static final String COL_PAYMENT_NAME = "payment";
	public static final String COL_PAYMENT_TYPE = TblTraveler.COL_BASE_PAYMENT_TYPE;
	public static final String COL_PAYMENT_TYPE_OPT = "NOT NULL";
	public static final String COL_PAYMENT_DESC = "Amount which we have to pay to traveler.";
	public static final int COL_PAYMENT_COLUMN_IDX = 4;

	// Column date -
	public static final String COL_DATE_NAME = "date";
	public static final String COL_DATE_TYPE = "INTEGER";
	public static final String COL_DATE_TYPE_OPT = "NOT NULL";
	public static final String COL_DATE_DESC = "Unix Time, the number of miliseconds since 1970-01-01 00:00:00 UTC";
	public static final int COL_DATE_COLUMN_IDX = 5;

	// Column payment
	// 0 = false, 1 = true
	public static final String COL_PAID_STATE_NAME = "paid_state";
	public static final String COL_PAID_STATE_TYPE = "INTEGER DEFAULT 0";
	public static final String COL_PAID_STATE_OPT = "NOT NULL";
	public static final int COL_PAID_STATE_COLUMN_IDX = 6;

	public static String createTableString() {
		String[][] allColParm = {
				{ COL_KEY_ID, COL_KEY_TYPE, COL_KEY_TYPE_OPT },
				{ COL_FROM_KEY_NAME, COL_FROM_KEY_TYPE, COL_FROM_KEY_TYPE_OPT },
				{ COL_TO_KEY_NAME, COL_TO_KEY_TYPE, COL_TO_KEY_TYPE_OPT },
				{ COL_TRAVELER_KEY_NAME, COL_TRAVELER_KEY_TYPE,
						COL_TRAVELER_KEY_TYPE_OPT },
				{ COL_PAYMENT_NAME, COL_PAYMENT_TYPE, COL_PAYMENT_TYPE_OPT },
				{ COL_DATE_NAME, COL_DATE_TYPE, COL_DATE_TYPE_OPT },
				{ COL_PAID_STATE_NAME, COL_PAID_STATE_TYPE, COL_PAID_STATE_OPT },
				{ "FOREIGN KEY", '(' + COL_FROM_KEY_NAME + ')', "REFERENCES ",
						TblCity.TAB_NAME + '(' + TblCity.COL_KEY_ID + ')' },
				{ "FOREIGN KEY", '(' + COL_TO_KEY_NAME + ')', "REFERENCES",
						TblCity.TAB_NAME + '(' + TblCity.COL_KEY_ID + ')' } };

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ");
		sb.append(TAB_NAME);
		sb.append(" ( ");
		for (int colIdx = 0; colIdx < allColParm.length; colIdx++) {
			for (int parmIdx = 0; parmIdx < allColParm[colIdx].length; parmIdx++) {
				sb.append(allColParm[colIdx][parmIdx]);
				if (parmIdx < allColParm[colIdx].length - 1)
					sb.append(' ');
			}
			if (colIdx < allColParm.length - 1)
				sb.append(", ");
		}
		sb.append(");");

		return sb.toString();
	}

	public static String dropTableString() {
		return "DROP TABLE IF EXISTS " + TAB_NAME;
	}

	public static String[] createRelationsString() {
		String[] value = new String[3];
		StringBuilder sb = new StringBuilder();
		sb.append("ALTER TABLE ").append(TAB_NAME).append(" ADD CONSTRAINT ")
				.append(COL_FROM_KEY_NAME).append(" FOREIGN KEY ").append("(")
				.append(COL_FROM_KEY_NAME).append(")").append(" REFERENCES ")
				.append(TblCity.TAB_NAME).append("(")
				.append(TblCity.COL_KEY_ID).append(")");

		value[0] = sb.toString();
		sb = new StringBuilder();
		sb.append("ALTER ").append(TAB_NAME).append(" ADD CONSTRAINT ")
				.append(COL_TO_KEY_NAME).append(" FOREIGN KEY ").append(" ( ")
				.append(COL_TO_KEY_NAME).append(" ) ").append(" REFERENCES ")
				.append(TblCity.TAB_NAME).append(" ( ")
				.append(TblCity.COL_KEY_ID).append(" ) ");

		value[1] = sb.toString();
		sb = new StringBuilder();
		sb.append("ALTER ").append(TAB_NAME).append(" ADD CONSTRAINT ")
				.append(COL_TRAVELER_KEY_NAME).append(" FOREIGN KEY ")
				.append(" ( ").append(COL_TRAVELER_KEY_NAME).append(" ) ")
				.append(" REFERENCES ").append(TblTraveler.TAB_NAME)
				.append(" ( ").append(TblTraveler.COL_KEY_ID).append(" ) ");

		value[2] = sb.toString();
		return value;

	}

	public static long insert(SQLiteDatabase _db, long _from_id, long _to_id,
			long _traveler_id, float _payment, long _date, boolean _payd_state) {
		ContentValues newRow = new ContentValues();
		newRow.put(COL_FROM_KEY_NAME, _from_id);
		newRow.put(COL_TO_KEY_NAME, _to_id);
		newRow.put(COL_TRAVELER_KEY_NAME, _traveler_id);
		newRow.put(COL_PAYMENT_NAME, _payment);
		newRow.put(COL_DATE_NAME, _date);
		newRow.put(COL_PAID_STATE_NAME, _payd_state ? 1 : 0);
		return _db.insert(TAB_NAME, null, newRow);
	}

	public static long insertTravel(SQLiteDatabase _db, TblTravel.Travel _travel) {
		return insert(_db, _travel.getFromId(), _travel.getToId(),
				_travel.getTravelerId(), _travel.getPayment(),
				_travel.getDate(), _travel.isPayd());
	}

	public static boolean update(SQLiteDatabase _db, long _travel_id,
			long _from_id, long _to_id, long _traveler_id, float _payment,
			long _date, boolean _payd_state) {
		String where = COL_KEY_ID + "=" + _travel_id;
		ContentValues newRow = new ContentValues();
		newRow.put(COL_FROM_KEY_NAME, _from_id);
		newRow.put(COL_TO_KEY_NAME, _to_id);
		newRow.put(COL_TRAVELER_KEY_NAME, _traveler_id);
		newRow.put(COL_PAYMENT_NAME, _payment);
		newRow.put(COL_DATE_NAME, _date);
		newRow.put(COL_PAID_STATE_NAME, _payd_state ? 1 : 0);
		return _db.update(TAB_NAME, newRow, where, null) > 0;
	}

	public static boolean deleteIdx(SQLiteDatabase _db, long _index) {
		String where = COL_KEY_ID + "=" + _index;
		return _db.delete(TAB_NAME, where, null) > 0;
	}

	public static void deleteAll(SQLiteDatabase _db) {
		throw new UnsupportedOperationException();
	}

	public static Cursor getAll(SQLiteDatabase _db) {
		String[] columns = { COL_KEY_ID, COL_FROM_KEY_NAME, COL_TO_KEY_NAME,
				COL_TRAVELER_KEY_NAME, COL_DATE_NAME, COL_PAID_STATE_NAME };
		return _db.query(TAB_NAME, columns, null, null, null, null, null);
	}

	public static Cursor get(SQLiteDatabase _db, long _idx) {
		String[] columns = { COL_KEY_ID, COL_FROM_KEY_NAME, COL_TO_KEY_NAME,
				COL_TRAVELER_KEY_NAME, COL_PAYMENT_NAME, COL_DATE_NAME,
				COL_PAID_STATE_NAME };
		String where = COL_KEY_ID + "=" + _idx;
		return _db.query(TAB_NAME, columns, where, null, null, null, null);
	}

	public static TblTravel.Travel getTravel(SQLiteDatabase _db, long _idx) {
		TblTravel.Travel travel = null;
		Cursor c = get(_db, _idx);
		if (c != null && c.moveToFirst())
			travel = new TblTravel.Travel(c.getLong(COL_KEY_COLUMN_IDX),
					c.getLong(COL_FROM_COLUMN_IDX),
					c.getLong(COL_TO_COLUMN_IDX),
					c.getLong(COL_TRAVELER_KEY_COLUMN_IDX),
					c.getFloat(COL_PAYMENT_COLUMN_IDX),
					c.getLong(COL_DATE_COLUMN_IDX),
					c.getInt(COL_PAID_STATE_COLUMN_IDX) > 0 ? true : false);
		return travel;
	}

	public static TblTravel.Travel getTravelTest(SQLiteDatabase _db, long _idx) {
		TblTravel.Travel travel = null;
		Cursor c = get(_db, _idx);
		if (c != null && c.moveToFirst())
			travel = new TblTravel.Travel(c.getLong(c
					.getColumnIndex(COL_KEY_ID)), c.getLong(c
					.getColumnIndex(COL_FROM_KEY_NAME)), c.getLong(c
					.getColumnIndex(COL_TO_KEY_NAME)), c.getLong(c
					.getColumnIndex(COL_TRAVELER_KEY_NAME)), c.getFloat(c
					.getColumnIndex(COL_PAYMENT_NAME)), c.getLong(c
					.getColumnIndex(COL_DATE_NAME)), c.getInt(c
					.getColumnIndex(COL_PAID_STATE_NAME)) > 0 ? true : false);
		return travel;
	}

	public List<TblTravel.Travel> getAllTravels(SQLiteDatabase _db) {
		Cursor c = getAll(_db);
		List<TblTravel.Travel> travels = new LinkedList<TblTravel.Travel>();
		if (c != null && c.moveToFirst())
			do {
				travels.add(new TblTravel.Travel(c.getLong(COL_KEY_COLUMN_IDX),
						c.getLong(COL_FROM_COLUMN_IDX), c
								.getLong(COL_TO_COLUMN_IDX), c
								.getLong(COL_TRAVELER_KEY_COLUMN_IDX), c
								.getFloat(COL_PAYMENT_COLUMN_IDX), c
								.getLong(COL_DATE_COLUMN_IDX), c
								.getInt(COL_PAID_STATE_COLUMN_IDX) > 0 ? true
								: false));
			} while (c.moveToNext());
		return travels;
	}

	public static class Travel {
		private final long id;
		private final long fromId;
		private final long toId;
		private final long travelerId;
		private final float payment;
		private final long date;
		private final boolean payd;

		/**
		 * 
		 * @param _id
		 * @param _fromId
		 * @param _toId
		 * @param _travelerId
		 * @param _payment
		 * @param _date
		 * @param _payd
		 */
		public Travel(long _id, long _fromId, long _toId, long _travelerId,
				float _payment, long _date, boolean _payd) {
			this.id = _id;
			this.fromId = _fromId;
			this.toId = _toId;
			this.travelerId = _travelerId;
			this.payment = _payment;
			this.date = _date;
			this.payd = _payd;
		}

		/**
		 * 
		 * @param _fromId
		 * @param _toId
		 * @param _travelerId
		 * @param _payment
		 * @param _date
		 * @param _payd
		 */
		public Travel(long _fromId, long _toId, long _travelerId,
				float _payment, long _date, boolean _payd) {
			this(-1l, _fromId, _toId, _travelerId, _payment, _date, _payd);
		}

		public long getId() {
			return id;
		}

		public long getFromId() {
			return fromId;
		}

		public long getToId() {
			return toId;
		}

		public float getPayment() {
			return payment;
		}

		public long getDate() {
			return date;
		}

		public boolean isPayd() {
			return payd;
		}

		public long getTravelerId() {
			return travelerId;
		}

		@Override
		public boolean equals(Object _o) {
			if (_o == this)
				return true;
			if (!(_o instanceof TblTravel.Travel))
				return false;
			TblTravel.Travel t = (TblTravel.Travel) _o;
			return this.id == t.id && this.fromId == t.fromId
					&& this.toId == t.toId && this.travelerId == t.travelerId
					&& this.payment == t.payment && this.date == t.date
					&& this.payd == t.payd;
		}

		@Override
		public int hashCode() {
			long value = 17;
			value = value * id + 3;
			value = value * fromId + 3;
			value = value * toId + 3;
			value = value * travelerId + 3;
			value = value * Float.floatToIntBits(payment) + 3;
			value = value * date + 3;
			value = value + (payd ? 1 : 0);
			return (int) (value ^ (value >>> 32));
		}

	}
}
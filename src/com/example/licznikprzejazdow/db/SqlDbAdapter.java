package com.example.licznikprzejazdow.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SqlDbAdapter {

	// ===========================================================
	// Constants
	// ===========================================================
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "db_lp_ver_1.db";

	private static final String DEBUG_TAG = "SqlDbAdapter";
	private final Context context;
	// ===========================================================
	// Fields
	// ===========================================================
	private MySqlHelper sqlHelper;
	private SQLiteDatabase db;
	private DbAdapterState state = DbAdapterState.NOT_OPPENED;

	// ===========================================================
	// Constructors
	// ===========================================================
	public SqlDbAdapter(Context _c) {
		this.context = _c;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	// ===========================================================
	// Methods
	// ===========================================================
	public SQLiteDatabase open() throws IllegalStateException {
		if (DbAdapterState.RO_OPENED == this.state
				|| DbAdapterState.RW_OPENED == this.state)
			throw new IllegalStateException("Trying to open opened data base.");

		this.sqlHelper = new MySqlHelper(this.context, DB_NAME, null,
				DB_VERSION);
		try {
			Log.d(DEBUG_TAG, "Opening database to read-write...");
			this.db = this.sqlHelper.getWritableDatabase();
			this.state = DbAdapterState.RW_OPENED;
		} catch (SQLException e) {
			Log.d(DEBUG_TAG, "Opening database to read-only...");
			this.db = this.sqlHelper.getReadableDatabase();
			this.state = DbAdapterState.RO_OPENED;
		}
		return this.db;
	}

	public void close() {
		if (this.sqlHelper != null)
			this.sqlHelper.close();
		this.state = DbAdapterState.CLOSED;
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	public final DbAdapterState getState() {
		return state;
	}

	public SQLiteDatabase getDb() {
		return db;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	public static enum DbAdapterState {
		NOT_OPPENED, RW_OPENED, RO_OPENED, CLOSED;
	}

	private class MySqlHelper extends SQLiteOpenHelper {

		public MySqlHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase _db) {
			// TODO Auto-generated method stub
			Log.d(DEBUG_TAG, "Creating database...");

			String tabCreateString;
			this.createTable(_db, TblTraveler.TAB_NAME,
					tabCreateString = TblTraveler.createTableString());
			System.out.println(tabCreateString);
			this.createTable(_db, TblCity.TAB_NAME,
					tabCreateString = TblCity.createTableString());
			System.out.println(tabCreateString);
			this.createTable(_db, TblTravel.TAB_NAME,
					tabCreateString = TblTravel.createTableString());
			System.out.println(tabCreateString);

			// for (String s : TblTravel.createRelationsString()){
			// System.out.println(s);
			// _db.execSQL(s);
			//
			// }

			Log.d(DEBUG_TAG, "Database is created :)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase _db, int arg1, int arg2) {
			// TODO Auto-generated method stub
			Log.d(DEBUG_TAG, "Data base updating does not work.");
		}

		private void createTable(SQLiteDatabase _db, String _tab,
				String _createStr) {
			Log.d(DEBUG_TAG, "Creating table: " + _tab);
			_db.execSQL(_createStr);
		}

		private void dropTable(SQLiteDatabase _db, String _tab, String _dropStr) {
			Log.d(DEBUG_TAG, "Dropping table: " + _tab);
			db.execSQL(_dropStr);
		}

	}

}

package com.example.licznikprzejazdow;

public interface ActivityTemple {
	// ===========================================================
	// Constants
	// ===========================================================

	// ===========================================================
	// Methods
	// ===========================================================

	public void onCreate_Attributes();

	public void onCreate_GuiComponents();

	public void onCreate_GuiListeners();

	public void onCreate_BroadcastListener();

}

package com.example.licznikprzejazdow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddNewCity extends Activity implements ActivityTemple {

	// ===========================================================
	// Constants
	// ===========================================================
	public static final String OUT_PARM_CITY_NAME = "opcn";

	// ===========================================================
	// Fields
	// ===========================================================
	private Button buttonAdd;
	private EditText editTextCityName;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new_city);

		this.onCreate_Attributes();
		this.onCreate_GuiComponents();
		this.onCreate_GuiListeners();
		this.onCreate_BroadcastListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_new_city, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreate_Attributes() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreate_GuiComponents() {
		// TODO Auto-generated method stub
		this.buttonAdd = (Button) this
				.findViewById(R.id.actAddNewCity_buttonAdd);
		this.editTextCityName = (EditText) this
				.findViewById(R.id.actAddNewCity_editTextCityName);

	}

	@Override
	public void onCreate_GuiListeners() {
		// TODO Auto-generated method stub
		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (arg0 == buttonAdd) {
					Intent i = getIntent();
					i.putExtra(OUT_PARM_CITY_NAME, editTextCityName.getText()
							.toString());
					setResult(RESULT_OK, i);
					finish();
				}
			}

		};
		this.buttonAdd.setOnClickListener(listener);
	}

	@Override
	public void onCreate_BroadcastListener() {
		// TODO Auto-generated method stub

	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
